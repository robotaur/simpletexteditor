package simpletexteditor;

/**
 *
 * @author Amante
 */
public class SimpleTextEditor {
    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(() -> {
            TextEditorUI app = new TextEditorUI();
            app.setVisible(true);
        });
    }
}
