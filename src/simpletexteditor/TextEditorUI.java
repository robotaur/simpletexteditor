package simpletexteditor;

import com.sun.glass.events.KeyEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Amante
 */
public class TextEditorUI extends JFrame {

    String fileName;
    File openFile;
    JEditorPane pane;
    boolean isEditing, newDocument;
    final FileNameExtensionFilter textDocumentFilter = new FileNameExtensionFilter("Text Documents (*.txt)", "txt");

    public TextEditorUI() {
        initUI();
        createMenuBar();
        initEditorPane();

        isEditing = false;
        newDocument = true;
    }

    private void initUI() {
        setFileName("Untitled Document");
        setSize(800, 600);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        JMenuItem newItem = new JMenuItem("New");
        newItem.setMnemonic(KeyEvent.VK_N);
        newItem.addActionListener(new NewAction());

        JMenuItem loadItem = new JMenuItem("Open Document...");
        loadItem.setMnemonic(KeyEvent.VK_O);
        loadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        loadItem.addActionListener(new LoadAction());

        JMenuItem saveItem = new JMenuItem("Save Changes...");
        saveItem.setMnemonic(KeyEvent.VK_S);
        saveItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        saveItem.addActionListener(new SaveAction());

        JMenuItem quitItem = new JMenuItem("Quit Application");
        quitItem.setMnemonic(KeyEvent.VK_Q);
        quitItem.addActionListener(new QuitAction());
        quitItem.setToolTipText("Quit application");
        quitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));

        fileMenu.add(newItem);
        fileMenu.add(loadItem);
        fileMenu.add(saveItem);
        fileMenu.addSeparator();
        fileMenu.add(quitItem);

        menuBar.add(fileMenu);

        setJMenuBar(menuBar);
    }

    private void initEditorPane() {
        pane = new JEditorPane();
        JScrollPane jsp = new JScrollPane(pane);
        add(jsp, BorderLayout.CENTER);

        pane.getDocument().addDocumentListener(new EditorListener());
    }

    void setFileName(String fileName) {
        this.fileName = fileName;
        this.setTitle(fileName);
    }

    void startEdit() {
        if (!this.isEditing) {
            this.isEditing = true;
            this.setTitle(fileName + "*");
        }
    }

    boolean showSaveFileDialog() throws Exception {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(textDocumentFilter);

        int result = fileChooser.showSaveDialog(null);

        // If yes => save and return true;
        // If no => don't save but return true;
        // If cance l=> don't save and return false;
        if (result == JFileChooser.APPROVE_OPTION) {
            saveFile(fileChooser.getSelectedFile());
            return true;
        } else {
            return false;
        }
    }

    void saveFile(File selectedFile) throws Exception {
        if (selectedFile == null && openFile == null) {
            throw new Exception("Something went horribly wrong");
        }

        if (selectedFile == null && openFile != null) {
            selectedFile = openFile;
        }

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(selectedFile));
            writer.write(pane.getText());

            isEditing = false;
            openFile = selectedFile;
            newDocument = false;
            setFileName(selectedFile.getName());

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    boolean showUnsavedChangesDialog(String title) {
        if (isEditing) {
            int result = JOptionPane.showConfirmDialog(null, "<html><span style='font-weight: normal;'>Do you want to save changes to <b>" + fileName + "</b> first?<br /><br />You will lose unsaved changes if you don't.</span></html>",
                    title, JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);

            if (result == JOptionPane.YES_OPTION) {
                try {
                    if (newDocument) {
                        return showSaveFileDialog();
                    } else {
                        saveFile(openFile);
                    }
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                    return false;
                }
            } else if (result == JOptionPane.CANCEL_OPTION) {
                return false;
            }
        }

        return true;
    }

    class QuitAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (showUnsavedChangesDialog("Quit Simple Text Editor")) {
                System.exit(0);
            }
        }
    }

    class NewAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (showUnsavedChangesDialog("New Document")) {
                pane.setText("");
                isEditing = false;
                newDocument = true;
                openFile = null;
                setFileName("Untitled Document");
            }
        }
    }

    class SaveAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                if (newDocument && isEditing) {
                    showSaveFileDialog();
                } else if (isEditing) {
                    saveFile(null);
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    class LoadAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            if (showUnsavedChangesDialog("Load Document")) {
                openFileSelector();
            }
        }

        private void openFileSelector() {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(textDocumentFilter);

            if (JOptionPane.OK_OPTION == fileChooser.showOpenDialog(null)) {
                loadFile(fileChooser.getSelectedFile().getPath());
            }
        }

        private void loadFile(String path) {
            try {
                File loaded = new File(path);
                String content = new Scanner(loaded).useDelimiter("\\A").next();
                pane.setText(content);

                openFile = loaded;
                isEditing = false;
                newDocument = false;
                setFileName(loaded.getName());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(TextEditorUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    class EditorListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent de) {
            startEdit();
        }

        @Override
        public void removeUpdate(DocumentEvent de) {
            startEdit();
        }

        @Override
        public void changedUpdate(DocumentEvent de) {
            startEdit();
        }
    }
}
